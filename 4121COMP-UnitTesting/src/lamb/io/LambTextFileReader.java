package lamb.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import lamb.model.Lamb;
import lamb.view.LambViewer;

/**
 * LambTextFileReader - the one stop shop for reading lamb-based statistics
 * 
 * @author david
 *
 */
public class LambTextFileReader implements LambReader {

	/** 
	 * @see lamb.io.LambReader#readLambs(java.lang.String)
	 */
	@Override
	public List<Lamb> readLambs(String file) throws FileNotFoundException {
		File flockFile = new File(file);
		FileInputStream fileStream = new FileInputStream(file);
		return readLambs(fileStream);
	}

	/** 
	 * @see lamb.io.LambReader#readLambs(java.io.InputStream)
	 */
	@Override
	public List<Lamb> readLambs(InputStream stream) {
		ArrayList<Lamb> flock = new ArrayList<>();

		Scanner scan = new Scanner(stream);

		while (scan.hasNextLine()) {
			Lamb newLamb = new Lamb(scan.nextLine(),false,  null);
			newLamb.gender = Lamb.Gender.valueOf(scan.nextLine().trim());

			String woollyness = scan.nextLine();
			newLamb.woolly = false;
			if (woollyness.equals("woolly"))
				newLamb.woolly = true;

			flock.add(newLamb);
		}

		return flock;
	}

}
