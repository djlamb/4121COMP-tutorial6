package lamb.io;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import lamb.model.Lamb;

/**
 * Interface that describes expected behaviour for a reader that can produce
 * Lamb objects from a given file or input stream
 * 
 * @author david
 */
public interface LambReader {

	/**
	 * Reads a list of lambs from a properly-formatted file
	 * 
	 * @param file
	 *            the path to the file
	 * @return the list of lambs (an empty, rather than null list, if the file
	 *         is empty)
	 * @throws FileNotFoundException
	 *             in accordance with {@link java.util.Scanner}
	 */
	List<Lamb> readLambs(String file) throws FileNotFoundException;

	/**
	 * Reads a list of lambs from any given input stream (e.g. the console, a
	 * file)
	 * 
	 * @param stream
	 *            the input stream
	 * @return the list of lambs (an empty, rather than null list, if the file
	 *         is empty)
	 * @throws FileNotFoundException
	 *             in accordance with {@link java.util.Scanner}
	 */
	List<Lamb> readLambs(InputStream stream);

}