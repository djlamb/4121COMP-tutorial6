package lamb.view;

import java.util.List;

import lamb.model.Lamb;
import lamb.model.LambSorter;

/**
 * A simple class that is responsible for showing details of Lamb objects
 * 
 * @author david
 *
 */
public class LambViewer {
	/**
	 * Shows a list of Lambs organised by woollyness.
	 * 
	 * @param flock
	 *            the list of lambs
	 */
	public void show(List<Lamb> flock) {
		LambSorter sorted = new LambSorter(flock);
		sorted.sortWoolly();
		
		System.out.println("Lambs--");
		for (Lamb lamb : sorted.getFlock()){
			if (lamb.woolly)
				System.out.print("Wooly: ");
			else 
				System.out.print("Shorn:");
			show(lamb);
		}
	}

	/**
	 * Displays some basic information about a Lamb object
	 * 
	 * @param lamb
	 *            the lamb object to display
	 */
	public void show(Lamb lamb) {
		System.out.println(lamb.name + " (" + lamb.gender + ")");
	}
}
